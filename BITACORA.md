# Prueba tecnica Front Ecomsur 2021

## Instalar y correr la aplicación
1. En la carpeta `root` de la aplicacion correr:
   `npm install`
2. Navega al directorio `front` y vuelve a correr el comando:
   `npm install`
3. Regresa al directorio root `cd ..`.
 

Toda la aplicación se podrá correr con un comando : 
`npm run dev`

Al momento de ejecutar el comando empezara a correr tanto el back como el front

El front :
`http://localhost:3000/`

El back: 
`http://localhost:5000/`

El back tiene varias bifurcaciones que son:

- La lista de productos se encuentra:
  `http://localhost:5000/api/products`

- Puedes encontrar cada producto por su ID:
  `http://localhost:5000/api/products/1`

- Las imágenes se encuentran en:|
  `http://localhost:5000/images/{{nombre-de-la-imagen}}`


## Descripción del problema de la prueba
Debia de tener 3 pantallas diferentes:

1. ProductListPage que debia ser la  pantalla principal. Esta debia tener una lista de productos
2. CartPage, esta pantalla debia mostrar los produuctos que el usuario hubiera escogido
3. ProductDisplayPage, esta ultima pantalla debia ser la descripción de cada producto, es decir, que en el momento que      dieran click encima del producto en la pantalla ProductListPage, los redirijiera a esta pantalla.

## ¿Como aborde el problema?
Maneje el estado global con un hook de react llamado useContext, asi podia solo hacer un llamado a la api y poder llamarla desde cualquier componente. Para poder enlazar mi aplicación para que fuera una single-page application lo lleve a cabo con una etiqueta llamada Link de react-router-dom. 
Hice uso de varios hooks de react, como useState, useEffect, useContext y tambien hice uso de useParams que es propio de react-router-dom para captar el id que me mandaban por medio de la url.
Trate de tener todo bien distribuido desde el orden de las carpetas y tambien que todo estuviera ordenado por componentes


## Feedback
Llegue a ser seleccionado o no, me gustaria que me pudieran dar feedback para asi poder mejorar como profesional y seguir aprendiendo cada vez mas ya que me gusta estar constantemente aprendiendo y sorprendiendome de cosas nuevas. Les agradeceria si me pudieran dar feedback. 

## !Muchas Gracias.¡ 