import { BrowserRouter, Switch, Route } from 'react-router-dom';
import CartPage from '../components/CartPage';
import Layout from '../components/Layout';
import ProductDisplayPage from '../components/ProductDisplayPage';
import ProductListPage from '../components/ProductListPage';
import AppContext from '../context/AppContext';
import useInitialState from '../hooks/useInitialState';


const App = () => {
  const initialState = useInitialState();
  return(
    <AppContext.Provider value={initialState}>
      <BrowserRouter>
        <Layout>
          <Switch>
            <Route exact path="/" component= {ProductListPage}/>
            <Route exact path="/product/cart" component={CartPage}/>
            <Route exact path="/product/information/:idItem" component={ProductDisplayPage}/>
          </Switch>
        </Layout>
      </BrowserRouter>
    </AppContext.Provider>
  )
}

export default App
