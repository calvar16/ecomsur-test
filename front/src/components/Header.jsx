import React, {useContext} from 'react'
import { Link } from 'react-router-dom'
import AppContext from '../context/AppContext'
import '../styles/components/Header.css'

const Header = () => {
   const {response} = useContext(AppContext)
   
   return (
      <div className="Header">
         <h1 className="Header-title">
            <Link to="/">
               Ecomsur Products
            </Link>
         </h1>
         <div className="Header-checkout">
            <Link to="/product/cart">
               <i className="fas fa-shopping-cart"></i>            
            </Link>
            {response && response.cart && response.cart.length > 0 ? <div className="Header-alert">{`carrito (${response.cart.length})`}</div> : <div className="Header-alert">{`carrito (0)`}</div>}
         </div>
      </div>
   )
}

export default Header
