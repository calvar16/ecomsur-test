import React, {useContext} from 'react'
import AppContext from '../context/AppContext';
import '../styles/components/CartPage.css'
import Orders from './Orders';

const CartPage = () => {
   const {response, removeFromCart} = useContext(AppContext)
   const {cart} = response;

   const  handleSumTotal = () => {
      const reducer = (accumulator, currentValue) => accumulator + currentValue.price;
      const sum = cart.reduce(reducer, 0)
      return sum;
   };

   const  handleRemoveToCart = (product) => () => {
      removeFromCart(product)
   }

   return (
      <div className="container-cart">
         <div className="container-content">
            {cart.length > 0 ? <h3>Tu carrito: </h3> : <Orders />}

            {cart.map((item) => (
               <div className="container-item-payment" key={item._id}>
                  <div className="container-element">
                     <h4>{item.name}</h4>
                     <span>$ {item.price}</span>
                  </div>
                  <button type="button" onClick={handleRemoveToCart(item)}>
                     <i className="fas fa-trash-alt"></i>
                  </button>
               </div>
            ))}
         </div>

         {cart.length > 0 && (
            <div className="container-sidebar">
               <h3>{`Precio Total: $ ${handleSumTotal()}`}</h3>
            </div>
         )}
      </div>
   )
}

export default CartPage
