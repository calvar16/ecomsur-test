import React,  {useContext} from 'react'
import AppContext from '../context/AppContext';
import InformationCard from './InformationCard';

const ProductDisplayPage = () => {
   const {response, addToCart} = useContext(AppContext)

   const handleAddToCart = product => () => {
      addToCart(product)
   }
   


   return (
      <div className="container-detail-item">
         <InformationCard 
            key={response.id}
            handleAddToCart={handleAddToCart}
            product={response}
         />
      </div>
   )
}

export default ProductDisplayPage
