import React from 'react'
import { Link } from 'react-router-dom';
import '../styles/components/ProductListPage.css'


const Product = ({item, handleAddToCart}) => {

   return (
      <div className="container-item" >
         <Link to={"/product/information/" + item._id}>
            <img src={`http://localhost:5000${item.image}`} alt={item.name}/>
            <div className="container-item-information">
               <h2>{item.name}</h2>
               <p>$ {item.price}</p>
               <p>{item.categories}</p>
            </div>
         </Link>
         <button type="button" onClick={handleAddToCart(item)}>Add item to cart</button>
      </div>
   )
}

export default Product
