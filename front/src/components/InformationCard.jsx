import React, {useEffect, useState, useContext} from 'react'
import { useParams } from 'react-router-dom';
import '../styles/components/InformationCard.css'

function InformationCard (props)  {
   const { idItem } = useParams()
   const [item, setItem] = useState([]);

   useEffect (() =>{
      fetch ("http://localhost:5000/api/products/" + idItem)
         .then(res => res.json())
         .then(res => setItem(res))
   }, [idItem])



   return (
      <div className="container">
         
            <div className="container-img">
               <img src={`http://localhost:5000/${item.image}`} alt={item.name}/>
            </div>
            <div className="container-information">
               <h2>{item.name}</h2>
               
               <p className="container-information__color">Descripción del producto :</p>
               <p>{item.description}</p>   
               
            </div>
            <div  className="container-information">
               <h3>Precio: <span>$ {item.price}</span></h3>
               <h3>Marca: <span>{item.brand}</span> </h3>
               <p>{`${item.rating} estrellas`}</p>
               <p>{`${item.numReviews} opiniones`}</p>   
               <div className="container-button">
                  <button type="button" onClick={props.handleAddToCart(item)}>Add item to cart</button>
               </div>
            </div>
      </div>
   )
}

export default InformationCard
