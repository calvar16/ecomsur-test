import React from 'react'
import { Link } from 'react-router-dom'
import '../styles/components/Orders.css'

const Orders = () => {
   return (
      <div className="container-orders">
         <img className="img-cart" src="https://thumbs.dreamstime.com/b/carro-vac%C3%ADo-del-supermercado-vector-105519210.jpg" alt="Cart" />
         <div className="container-orders-info">
            <h2>No tienes pedidos aún</h2>
            <Link to="/">
               <div className="link">
                  Ver Productos
               </div>
            </Link>
         </div>
      </div>
   )
}

export default Orders
