import React, { useContext, useState } from 'react'
import '../styles/components/ProductListPage.css'
import Product from './Product'
import AppContext from '../context/AppContext'

const ProductListPage = () => {
   const { response, addToCart } = useContext(AppContext)
   const [text, setText] = useState('');
   

   const handleOnChange= (e) => {
      setText(e.target.value)
   }

   const filter = (val) => { 
      if(text === ""){
         return val
      }else if(val.name.toLowerCase().includes(text.toLowerCase()) || val.brand.toLowerCase().includes(text.toLowerCase()) ){
         return val
      }
    }

    const handleAddToCart = product => () => {
      addToCart(product)
   }
   
   return (
      <div className="container-main">
         <div className="container-input">
            <input 
               type="text"
               placeholder="Buscar ..."
               className="input" 
               value={text}
               onChange={handleOnChange}  
            />
         </div>
         
         <div className="container">
            {response && response.products && response.products.filter(filter).map((item) => (
               <Product 
                  key={item._id} 
                  item={item} 
                  handleAddToCart={handleAddToCart}
               />
            ))}
         </div>
         
      </div>
   )
}

export default ProductListPage
