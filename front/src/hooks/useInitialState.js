import { useState, useEffect } from 'react';

const useInitialState = () => {
  const [response, setResponse] = useState([])

  useEffect(async () => {
    await fetch("http://localhost:5000/api/products")
      .then(res => res.json())
      .then(res =>{
         setResponse(res)
      })
  }, [])
  
  const addToCart = (payload) => {
    debugger
    setResponse({
      ...response,
      cart: [ ...response.cart, payload],
    });
    debugger
  }

  const removeFromCart = payload => {
    debugger
    setResponse({
      ...response,
      cart: response.cart.filter(items => items._id !== payload._id)
    });
    debugger
  };
  
  return {
    addToCart,
    removeFromCart,
    response,
  };
};

export default useInitialState;